package com.uni.david.accelerologger;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log extends AppCompatActivity implements SensorEventListener {

    // UI Objects
    Toolbar tBar;
    TextView xVal;
    TextView yVal;
    TextView zVal;
    TextView time;
    ToggleButton startStop;

    // Stores the time recording starts
    long startTime;

    // Sensor Objects
    SensorManager sensorManager;
    Sensor accelSensor;

    File externFile = null;
    BufferedWriter bw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        // Setup toolbar
        this.tBar = (Toolbar)findViewById(R.id.my_toolbar);
        this.setSupportActionBar(tBar);

        // Get UI Objects from the View
        this.xVal = (TextView) this.findViewById(R.id.xAccelBox);
        this.yVal = (TextView) this.findViewById(R.id.yAccelBox);
        this.zVal = (TextView) this.findViewById(R.id.zAccelBox);
        this.time = (TextView) this.findViewById(R.id.timeBox);
        this.startStop = (ToggleButton) this.findViewById(R.id.start_stop);

        // Get instance of SensorManager and the default linear Acceleration sensor
        this.sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        accelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        // Check to see if sensor is present
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) == null){
            // Warn user and disable start button
            this.xVal.setText("No");
            this.yVal.setText("Sensor");
            this.zVal.setText("Present");
            this.startStop.setEnabled(false);
        }

        this.startStop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Button activated
                    startSensor();
                } else {
                    // Button deactivated
                    stopSensor();
                }
            }
        });
    }

    // Small helper method to start the sensor
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void startSensor(){
        if(accelSensor != null) {
            // TODO modifiable delay
            sensorManager.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_NORMAL);

            if(this.isExternalStorageWritable()){
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmm");
                String fileName = (dateFormat.format(new Date()));
                fileName += "_DATA.csv";

                File parentDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS);

               this.externFile = new File(parentDir, fileName);

                try {
                    this.bw = new BufferedWriter(new FileWriter(this.externFile));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            this.startTime = SystemClock.elapsedRealtimeNanos();
        }
    }

    // Small helper method to stop the sensor
    private void stopSensor(){
        if(accelSensor != null) {
            sensorManager.unregisterListener(this);

            if(this.externFile != null){
                try {
                    this.bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        // The light sensor returns a single value.
        float x = event.values[0];
        this.xVal.setText(String.valueOf(x));
        float y = event.values[1];
        this.yVal.setText(String.valueOf(y));
        float z = event.values[2];
        this.zVal.setText(String.valueOf(z));

        double diff = (event.timestamp - this.startTime)/1000000.0;

        // Update time
        this.time.setText(String.valueOf(diff));


        if(this.externFile != null){
            try {
                this.bw.write(diff + "," + x  + "," +  y  + "," + z);
                this.bw.newLine();
            } catch (IOException e) {
                this.externFile = null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;

    }
}
